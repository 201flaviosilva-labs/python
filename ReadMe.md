# Python

## Description

Just simple projects to learn python and libs

Preview (WIKI): https://bitbucket.org/201flaviosilva-labs/python/wiki/Home

## Links

- [Source Code](https://bitbucket.org/201flaviosilva-labs/python/);
- [GitHub](https://github.com/201flaviosilva/);
- [GitLab](https://gitlab.com/201flaviosilva);
- [Replit](https://replit.com/@201flaviosilva);
- [Solo Learn](https://www.sololearn.com/profile/7859587);
