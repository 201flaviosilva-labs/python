# Snake AI

## Description
Train AI to Play Snake – Reinforcement Learning Course (Python, PyTorch, Pygame).

In this Python Reinforcement Learning course you will learn how to teach an AI to play Snake! We build everything from scratch using Pygame and PyTorch.

## Links
- [Tutorial](https://www.youtube.com/watch?v=L8ypSXwyBds);