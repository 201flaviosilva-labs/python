import pygame

from Game import Game
from Agent import Agent

from helper import plot


def train():
    plot_scores = []
    plot_mean_scores = []
    total_score = 0
    record = 0
    agent = Agent()
    game = Game()
    while True:
        state_old = agent.get_state(game)  # get old state
        final_move = agent.get_action(state_old)  # get move

        # perform move and get new state
        reward, game_over, score = game.update(final_move)
        state_new = agent.get_state(game)

        agent.train_short_memory(state_old, final_move, reward, state_new, game_over)  # train short memory

        agent.remember(state_old, final_move, reward, state_new, game_over)

        # Game Over - Long train
        if game_over:
            game.reset()
            agent.n_games += 1
            agent.train_long_memory()

            if score > record:
                record = score
                agent.model.save()

            print('Game', agent.n_games, 'Score', score, 'Record:', record)

            plot_scores.append(score)
            total_score += score
            mean_score = total_score / agent.n_games
            plot_mean_scores.append(mean_score)
            plot(plot_scores, plot_mean_scores)

        # Exit
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        pass


def main():
    if __name__ == "__main__": train()


main()
