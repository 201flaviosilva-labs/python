import pygame
import random
from enum import Enum
from collections import namedtuple
import numpy as np
import time

# https://www.youtube.com/watch?v=L8ypSXwyBds

pygame.init()
font = pygame.font.Font('arial.ttf', 25)


class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4


Point = namedtuple('Point', 'x, y')

# rgb colors
WHITE = (255, 255, 255)
RED = (200, 0, 0)
BLUE1 = (0, 0, 255)
BLUE2 = (0, 100, 255)
BLACK = (0, 0, 0)

BLOCK_SIZE = 20
SPEED = 20


class Game:
    def __init__( self, w=800, h=600 ):
        self.w = w
        self.h = h

        # init display
        self.display = pygame.display.set_mode((self.w, self.h))
        pygame.display.set_caption('Snake')
        self.clock = pygame.time.Clock()

        self.start_time = time.time()

        self.reset()

    def reset( self ):
        self.start_game_time = time.time()

        self.direction = Direction.RIGHT

        self.head = Point(self.w / 4, self.h / 2)
        self.snake = [self.head,
                      Point(self.head.x - BLOCK_SIZE, self.head.y),
                      Point(self.head.x - (2 * BLOCK_SIZE), self.head.y)]

        self.score = 0
        self.food = None
        self._place_food()
        self.frame_iteration = 0

    def _place_food( self ):
        x = random.randint(0, (self.w - BLOCK_SIZE) // BLOCK_SIZE) * BLOCK_SIZE
        y = random.randint(0, (self.h - BLOCK_SIZE) // BLOCK_SIZE) * BLOCK_SIZE
        self.food = Point(x, y)
        if self.food in self.snake: self._place_food()

    def update( self, action ):
        self.frame_iteration += 1

        # 2. move
        self._move(action)  # update the head
        self.snake.insert(0, self.head)

        # 3. check if game over
        reward = 0
        game_over = False
        isDeadCicle = self.frame_iteration > 100 * len(self.snake)
        if self.is_collision() or isDeadCicle:
            reward = -10
            game_over = True
            return reward, game_over, self.score

        # 4. place new food or just move
        if self.head == self.food:
            reward = 10
            self.score += 1
            self._place_food()
        else: self.snake.pop()

        # 5. update ui and clock
        self._draw()
        self.clock.tick(SPEED)

        # 6. return game over and score
        return reward, game_over, self.score

    def is_collision( self, point=None ):
        if (point == None): point = self.head
        # hits boundary
        if point.x > self.w - BLOCK_SIZE or point.x < 0 or point.y > self.h - BLOCK_SIZE or point.y < 0:
            return True
        # hits itself
        if point in self.snake[1:]: return True

        return False

    def _move( self, action ):
        # [straightm right, left]

        clock_wise = [Direction.RIGHT, Direction.DOWN, Direction.LEFT, Direction.UP]
        index = clock_wise.index(self.direction)

        if np.array_equal(action, [1, 0, 0]): self.direction = clock_wise[index]  # No change
        elif np.array_equal(action, [0, 1, 0]): self.direction = clock_wise[(index + 1) % 4]  # Right
        elif np.array_equal(action, [0, 0, 1]): self.direction = clock_wise[(index - 1) % 4]  # Left

        x = self.head.x
        y = self.head.y

        if self.direction == Direction.RIGHT: x += BLOCK_SIZE
        elif self.direction == Direction.LEFT: x -= BLOCK_SIZE
        elif self.direction == Direction.DOWN: y += BLOCK_SIZE
        elif self.direction == Direction.UP: y -= BLOCK_SIZE

        self.head = Point(x, y)

    def _draw( self ):
        self.display.fill(BLACK)

        for pt in self.snake:
            pygame.draw.rect(self.display, BLUE1, pygame.Rect(pt.x, pt.y, BLOCK_SIZE, BLOCK_SIZE))
            pygame.draw.rect(self.display, BLUE2, pygame.Rect(pt.x + 4, pt.y + 4, 12, 12))

        pygame.draw.rect(self.display, RED, pygame.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE))

        # Score
        score_label = font.render("Score: " + str(self.score), True, WHITE)
        self.display.blit(score_label, [0, 0])

        # Timer
        timer_label = font.render("Time: " + str(int(time.time() - self.start_time)), True, WHITE)
        self.display.blit(timer_label, [0, 50])

        timer_label = font.render("Time G: " + str(int(time.time() - self.start_game_time)), True, WHITE)
        self.display.blit(timer_label, [0, 100])

        pygame.display.flip()
