from tkinter import *

from tkinter.ttk import *

window = Tk()

window.title("Welcome to LikeGeeks app")

window.geometry('350x200')

#Label 1

lbl1 = Label(window, text="Combo 1:")

lbl1.grid(column=0, row=0)

#ComboBox 1

combo1 = Combobox(window)

combo1['values']= (1, 2, 3, 4, 5)

combo1.current(0)

combo1.grid(column=1, row=1)

#Label 2

lbl2 = Label(window, text="Combo 2:")

lbl2.grid(column=0, row=2)

#ComboBox 2

combo2 = Combobox(window)

combo2['values']= (1, 2, 3, 4, 5)

combo2.current(0)

combo2.grid(column=1, row=3)

#Label 3

lbl3 = Label(window, text="Resposta a 1:")

lbl3.grid(column=0, row=4)

#Label 4

lbl4 = Label(window, text="Resposta a 2:")

lbl4.grid(column=0, row=5)

#Label 5

lbl5 = Label(window, text="000")

lbl5.grid(column=1, row=4)

#Label 6

lbl6 = Label(window, text="000")

lbl6.grid(column=1, row=5)

#Botão

def clicked():

    res1 = combo1.get()

    lbl5.configure(text= res1)

    res2 = combo2.get()

    lbl6.configure(text= res2)
    

btn = Button(window, text="Clica", command=clicked)

btn.grid(column=2, row=2)

window.mainloop()
