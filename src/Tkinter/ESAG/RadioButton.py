from tkinter import *

from tkinter.ttk import *

window = Tk()

window.title("Olá Bem Vindo")

selected = IntVar()

rad1 = Radiobutton(window,text='Primeiro', value=1, variable=selected)

rad2 = Radiobutton(window,text='Segundo', value=2, variable=selected)

rad3 = Radiobutton(window,text='Terceiro', value=3, variable=selected)

lbl1 = Label(window, text="Olá")

lbl1.grid(column=0, row=1)

def clicked():

    lbl1.configure(text = selected.get())
    print(selected.get())

btn = Button(window, text="Clica", command=clicked)

rad1.grid(column=0, row=0)

rad2.grid(column=1, row=0)

rad3.grid(column=2, row=0)

btn.grid(column=3, row=0)

window.mainloop()
