from tkinter import *

from tkinter.ttk import *

window = Tk()

window.title("Welcome to LikeGeeks app")

window.geometry('350x200')

# Label 1

lbl1 = Label(window, text="Num 1:")

lbl1.grid(column=0, row=0)

# TextBox 1

txt1 = Entry(window, width=10)

txt1.grid(column=1, row=1)

# Label 2

lbl2 = Label(window, text="Num 2:")

lbl2.grid(column=0, row=2)

# TextBox 2

txt2 = Entry(window, width=10)

txt2.grid(column=1, row=3)

# Label 3

lbl3 = Label(window, text="Resposta:")

lbl3.grid(column=0, row=4)

# Label 7

lbl7 = Label(window, text="000")

lbl7.grid(column=0, row=5)

# Botão


def clicked():

    res1 = txt1.get()

    res2 = txt2.get()

    soma = int(res1)+int(res2)

    lbl7.configure(text=soma)


btn = Button(window, text="Clica", command=clicked)

btn.grid(column=2, row=2)

window.mainloop()
