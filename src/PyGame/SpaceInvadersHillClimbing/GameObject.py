import pygame
import math
from random import randint


class GameObject(object):
    def __init__(self, screen):
        # Screen
        self.screen = screen

        # Display, Active
        self.alive = True
        self.visible = True
        self.image = pygame.image.load("Assets/Spites/DebugBox/DebugBox.png")

        # Size
        self.width = 32
        self.height = 32

        # Position
        self.x = 0
        self.y = 0

        # Speed
        self.speedX = 0  # Actual Speed X
        self.speedY = 0  # Actual Speed Y

        # Collider
        self.collider = pygame.Rect(self.x, self.y, self.width, self.height)
        self.collideWithWorldBounds = False
        self.debug = True

        self.tag = ""

        self.generate()

    def generate(self):
        pass

    # Image/Texture
    def setTexture(self, texture):
        image = pygame.image.load(texture)
        self.image = image

    def setTexture(self, texture, width, height):
        image = pygame.image.load(texture)
        image = pygame.transform.scale(image, (width, height))
        self.image = image
        self.setSize(width, height)

    def setSize(self, width, height):
        self.width = width
        self.height = height

    # Position
    def setPosition(self, x, y):
        if not self.alive:
            return
        self.x = x
        self.y = y

    def setPositionX(self, newX):
        self.setPosition(newX, self.y)

    def setPositionY(self, newY):
        self.setPosition(self.x, newY)

    def setRandomPosition(self):
        x, y = self.screen.get_size()
        newX = randint(0, x - self.width)
        newY = randint(0, y - self.height)
        self.setPosition(newX, newY)

    def setRandomPosition(self, maxX, maxY):
        x, y = self.screen.get_size()
        newX = randint(0, maxX)
        newY = randint(0, maxY)
        self.setPosition(newX, newY)

    # Velocity
    def setVelocityX(self):
        if not self.alive:
            return
        x, y = self.screen.get_size()
        newX = self.x + self.speedX
        if (newX < 0 or x < newX + self.width) and self.collideWithWorldBounds:
            return
        self.setPositionX(newX)

    def setVelocityY(self):
        if not self.alive:
            return
        x, y = self.screen.get_size()
        newY = self.y + self.speedY
        if (newY <= 0 or y <= newY + self.height) and self.collideWithWorldBounds:
            return
        self.setPositionY(newY)

    # Check if collide with world/screen bounds
    def isCollidBondsX(self):
        x, y = self.screen.get_size()
        isCollide = False
        if math.floor(self.x) <= 0 or x <= math.ceil(self.x) + self.width:
            isCollide = True
        return isCollide

    def isCollidBondsY(self):
        x, y = self.screen.get_size()
        isCollide = False
        if math.floor(self.y) <= 0 or y <= math.ceil(self.y) + self.height:
            isCollide = True
        return isCollide

    def updateCollider(self):
        self.collider = pygame.Rect(self.x, self.y, self.width, self.height)
        if self.debug:
            pygame.draw.rect(self.screen, (0, 255, 0), self.collider, 1, 1)

    # Update - Draw
    def update(self):
        if not self.alive:
            return
        self.setVelocityX()
        self.setVelocityY()
        self.screen.blit(self.image, (self.x, self.y))
        self.updateCollider()

    # Destroy
    def destroy(self):
        self.alive = False
        self._destroy()

    def _destroy(self):
        pass
