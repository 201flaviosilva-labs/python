import pygame
from GameObject import GameObject


class Player(GameObject):
    def generate(self):
        # Image
        width = 66
        height = 45
        texture = "./Assets/Spites/SpaceShip.png"
        self.setTexture(texture, width, height)

        x, y = self.screen.get_size()

        self.x = x / 2 - width / 2
        self.y = y - height - 20

        self.steepX = 0.5  # Speed

    def _destroy(self):
        shootSound = pygame.mixer.Sound("Assets/Audio/Explosion.mp3")
        shootSound.play()
