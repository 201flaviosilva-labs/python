import pygame
from GameObject import GameObject


class Enemy(GameObject):
    def generate(self):
        # Image
        width = 45
        height = 32
        texture = "./Assets/Spites/InvasorIcon.png"
        self.setTexture(texture, width, height)

        x, y = self.screen.get_size()

        self.x = x / 2 - width / 2
        self.y = height + 20

        self.speedX = 0.3

        self.steepY = 10

    def _destroy(self):
        shootSound = pygame.mixer.Sound("Assets/Audio/Explosion.mp3")
        shootSound.play()
