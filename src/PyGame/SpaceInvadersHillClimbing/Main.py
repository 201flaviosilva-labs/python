import pygame

from Bullet import Bullet
from Player import Player
from Enemy import Enemy
from Label import Label


class Game:
    def __init__(self):
        pygame.init()

        # Screen
        self.screenWidth = 800
        self.screenHeight = 600
        self.screen = pygame.display.set_mode(
            (self.screenWidth, self.screenHeight))

        # Icon
        icon = pygame.image.load("Assets/Spites/InvasorIcon.png")
        pygame.display.set_caption("Space Invaders")
        pygame.display.set_icon(icon)

        # Loop
        self.running = True

        # Player
        self.player = Player(self.screen)

        # Bullets
        self.bullet = Bullet(self.screen)

        # Enemies
        self.numEnemies = 6
        self.enemies = []

        # Label
        self.scoreLabel = Label(self.screen)

        # Score
        self.score = 0

        self.create()
        self.update()

    def create(self):
        # Bullet
        self.bullet = Bullet(self.screen)
        self.bullet.alive = False

        # Labels
        txt = "Score: " + str(self.score)
        self.scoreLabel.setText(txt)
        self.scoreLabel.setPosition(0, 20)

        # Create Enemies
        for i in range(self.numEnemies):
            enemy = Enemy(self.screen)
            enemy.setRandomPosition(self.screenWidth - enemy.width, 100)
            self.enemies.append(enemy)

    def isColliding(self, coll1, coll2):
        return coll1.collider.colliderect(coll2.collider)

    def draw(self):  # Draw Screen
        self.screen.fill((122, 122, 122))

        if self.player.alive:
            self.player.update()
        for enemy in self.enemies:
            if enemy.alive:
                enemy.update()
        if self.bullet.alive:
            self.bullet.update()
        self.scoreLabel.update()

        pygame.display.flip()  # Update all screen

    def update(self):
        while self.running:
            for event in pygame.event.get():
                # Exit
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or event.type == pygame.QUIT:
                    self.running = False

                # Enable Move
                # Key Down
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:  # Left
                        self.player.speedX = -self.player.steepX
                    if event.key == pygame.K_RIGHT:  # Right
                        self.player.speedX = self.player.steepX
                    if event.key == pygame.K_SPACE \
                            and self.bullet.alive == False \
                            and self.player.alive:  # Space
                        self.bullet.create(self.player)

                if event.type == pygame.KEYUP:  # Key Up
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        self.player.speedX = 0

            # Move
            for enemy in self.enemies:
                if enemy.isCollidBondsX():
                    enemy.speedX = -enemy.speedX
                    enemy.setPositionY(enemy.y + enemy.steepY)

            if self.bullet.alive:
                self.bullet.shouldDestroy()
                for enemy in self.enemies:
                    if self.isColliding(enemy, self.bullet):
                        self.bullet.destroy()
                        enemy.destroy()
                        self.enemies.remove(enemy)
                        self.score += 1
                        self.scoreLabel.setText("Score: " + str(self.score))

            for enemy in self.enemies:
                if enemy.y > self.screenHeight:
                    enemy.setRandomPosition(
                        self.screenWidth - enemy.width, 100)
                    self.score -= 1
                    self.scoreLabel.setText("Score: " + str(self.score))

            self.draw()

        # Exit
        pygame.quit()
        exit()


game = Game()
