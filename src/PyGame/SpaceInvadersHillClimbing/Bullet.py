import pygame
from GameObject import GameObject


class Bullet(GameObject):
    def generate(self):
        # Image
        width = 9
        height = 20
        texture = "./Assets/Spites/Flame.png"
        self.setTexture(texture, width, height)

        x, y = self.screen.get_size()

        self.x = x / 2 - width / 2
        self.y = y + 100

        self.speedY = -3

    def shootSound(self):
        shootSound = pygame.mixer.Sound("Assets/Audio/Shoot.mp3")
        shootSound.play()

    def create(self, player):
        self.alive = True
        x = player.x + player.width / 2
        y = player.y
        self.setPosition(x, y)
        self.shootSound()

    def shouldDestroy(self):
        isCollide = self.isCollidBondsY()
        if isCollide:
            self.alive = False

    def _destroy(self):
        x, y = self.screen.get_size()
        self.y = y + 1000
