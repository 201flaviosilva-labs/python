import math
import random
import pygame

from Cube import Cube


class Snake(object):
    def __init__(self, screen, pos):
        self.body = []
        self.turns = {}

        self.screen = screen
        self.color = (255, 0, 0)
        self.head = Cube(screen, pos)
        self.body.append(self.head)
        self.directionX = 0
        self.directionY = 1

    def move(self, keys):
        for key in keys:
            if keys[pygame.K_LEFT]:
                self.directionX = -1
                self.directionY = 0
                self.turns[self.head.position[:]] = [self.directionX, self.directionY]

            elif keys[pygame.K_RIGHT]:
                self.directionX = 1
                self.directionY = 0
                self.turns[self.head.position[:]] = [self.directionX, self.directionY]

            elif keys[pygame.K_UP]:
                self.directionX = 0
                self.directionY = -1
                self.turns[self.head.position[:]] = [self.directionX, self.directionY]

            elif keys[pygame.K_DOWN]:
                self.directionX = 0
                self.directionY = 1
                self.turns[self.head.position[:]] = [self.directionX, self.directionY]

        for index, cell in enumerate(self.body):
            position = cell.position[:]
            if position in self.turns:
                turn = self.turns[position]
                cell.move(turn[0], turn[1])
                if index == len(self.body) - 1:
                    self.turns.pop(position)
            else:
                if cell.directionX == -1 and cell.position[0] <= 0:
                    cell.position = (cell.rows - 1, cell.position[1])
                elif cell.directionX == 1 and cell.position[0] >= cell.rows - 1:
                    cell.position = (0, cell.position[1])
                elif cell.directionY == 1 and cell.position[1] >= cell.rows - 1:
                    cell.position = (cell.position[0], 0)
                elif cell.directionY == -1 and cell.position[1] <= 0:
                    cell.position = (cell.position[0], cell.rows - 1)
                else:
                    cell.move(cell.directionX, cell.directionY)

    def reset(self, position):
        self.body = []
        self.turns = {}
        
        self.head = Cube(self.screen, (10, 10))
        self.body.append(self.head)
        self.directionX = 0
        self.directionY = 1

    def addCube(self):
        tail = self.body[-1]
        directionX, directionY = tail.directionX, tail.directionY

        if directionX == 1 and directionY == 0:
            self.body.append(Cube(self.screen, (tail.position[0] - 1, tail.position[1])))
        elif directionX == -1 and directionY == 0:
            self.body.append(Cube(self.screen, (tail.position[0] + 1, tail.position[1])))
        elif directionX == 0 and directionY == 1:
            self.body.append(Cube(self.screen, (tail.position[0], tail.position[1] - 1)))
        elif directionX == 0 and directionY == -1:
            self.body.append(Cube(self.screen, (tail.position[0], tail.position[1] + 1)))

        self.body[-1].directionX = directionX
        self.body[-1].directionY = directionY

    def draw(self, head=False):
        for index, cell in enumerate(self.body):
            if index == 0:
                cell.draw(True)
            else:
                cell.draw()
