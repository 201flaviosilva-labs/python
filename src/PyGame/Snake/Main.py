import math
import random
import pygame

from Grid import Grid
from Snake import Snake
from Cube import Cube
from Label import Label

class Game:
    def __init__(self):
        pygame.init()
        self.screenSize = 500

        self.screen = pygame.display.set_mode((self.screenSize, self.screenSize))

        self.rows = 20
        self.score = 0

        self.running = True
        self.create()
        self.update()

    def create(self):
        self.grid = Grid(self.screen, self.screenSize, self.rows)
        self.snake = Snake(self.screen, (10, 10))
        self.apple = object
        self.randomApple()
        self.label = Label(self.screen)
        self.label.setText("Pontos: {}".format(self.score))
        self.clock = pygame.time.Clock()
        pass

    def randomApple(self):
        global checkPosition
        freeCell = True
        positions = self.snake.body

        while freeCell:
            checkPosition = (random.randrange(self.rows), random.randrange(self.rows))
            if len(list(filter(lambda z: z.position == checkPosition, positions))) > 0:
                continue
            else:
                break

        self.apple = Cube(self.screen, checkPosition, color=(0, 255, 0))

    def update(self):
        while self.running:
            pygame.time.delay(50)
            self.clock.tick(10)

            keys = pygame.key.get_pressed()
            self.snake.move(keys)

            for event in pygame.event.get():
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or event.type == pygame.QUIT:  # Exit
                    self.running = False

            if self.snake.head.position == self.apple.position:
                self.score += 1
                self.label.setText("Pontos: {}".format(self.score))
                self.snake.addCube()
                self.randomApple()

            if self.checkCollisionBodySnekPosition(self.snake.head.position, True):
                self.score = 0
                self.label.setText("Pontos: {}".format(self.score))
                self.snake.reset((10, 10))

            self.draw()

        # End While

        pygame.quit()
        exit()

    def checkCollisionBodySnekPosition(self, checkPosition, skipHead=False):
        isColliding = False
        for index in range(len(self.snake.body)):
            if skipHead and index == 0:
                continue
            elif self.snake.body[index].position == checkPosition:
                isColliding = True
                break

        return isColliding

    def draw(self):  # Draw Screen
        self.screen.fill((122, 122, 122))
        self.grid.draw()
        self.label.draw()
        self.snake.draw()
        self.apple.draw()
        pygame.display.flip()


game = Game()
