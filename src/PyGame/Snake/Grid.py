import math
import random
import pygame


class Grid(object):
    def __init__(self, screen, size, rows):
        self.screen = screen
        self.size = size
        self.rows = rows

        self.cellSize = self.size // self.rows

    def draw(self):
        for i in range(self.rows):
            for j in range(self.rows):
                x = i * self.cellSize
                y = j * self.cellSize
                
                cell = pygame.Rect(x, y, self.cellSize, self.cellSize)
                pygame.draw.rect(self.screen, (100, 100, 100), cell, 1, 1)
