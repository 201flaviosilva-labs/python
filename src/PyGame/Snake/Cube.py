import math
import random
import pygame


class Cube(object):
    rows = 20
    screenSize = 500

    def __init__(self, screen, position, directionX=1, directionY=0, color=(255, 0, 0)):
        self.screen = screen
        self.position = position
        self.directionX = directionX
        self.directionY = directionY
        self.color = color

        self.screenSize = 500
        self.rows = 20
        self.cellSize = self.screenSize // self.rows

    def move(self, directionX, directionY):
        self.directionX = directionX
        self.directionY = directionY
        self.position = (self.position[0] + self.directionX, self.position[1] + self.directionY)

    def draw(self, head=False):
        x = self.position[0] * self.cellSize + 1
        y = self.position[1] * self.cellSize + 1

        cell = pygame.Rect(x, y, self.cellSize - 2, self.cellSize - 2)

        if head:
            pygame.draw.rect(self.screen, (0, 0, 255), cell)
        else:
            pygame.draw.rect(self.screen, self.color, cell)
