import pygame
from GameObject import GameObject


class Enemy(GameObject):
    def __init__(self, screen):
        width = 45
        height = 32

        image = pygame.image.load("Assets/Spites/InvasorIcon.png")

        x, y = screen.get_size()

        self.alive = True
        self.width = width
        self.height = height
        self.screen = screen
        self.image = image
        self.x = x / 2 - width / 2
        self.y = height + 20
        self.speedX = 0.3
        self.speedY = 0
        self.collideWithWorldBounds = True

        self.steepY = 10
