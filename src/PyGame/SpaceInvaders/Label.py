import pygame


class Label:
    def __init__(self, screen):
        x, y = screen.get_size()

        self.screen = screen
        self.font = pygame.font.Font(
            "./Assets/Fonts/Press_Start_2P/PressStart2P-Regular.ttf", 16)
        self.text = ""
        self.color = (255, 255, 255)
        self.x = x / 2
        self.y = y / 2

    def setText(self, text):
        self.text = text

    def setPosition(self, x, y):
        self.x = x
        self.y = y

    def update(self):
        label = self.font.render(str(self.text), True, self.color)
        self.screen.blit(label, (self.x, self.y))
