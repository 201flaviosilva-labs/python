import pygame
from GameObject import GameObject


class Bullet(GameObject):
    def __init__(self, screen):
        width = 9
        height = 20

        image = pygame.image.load("Assets/Spites/Flame.png")
        image = pygame.transform.scale(image, (width, height))

        x, y = screen.get_size()

        self.alive = True
        self.width = width
        self.height = height
        self.screen = screen
        self.image = image
        self.x = x / 2 - width / 2
        self.y = -100
        self.speedX = 0
        self.speedY = -3
        self.collideWithWorldBounds = False

    def shootSound(self):
        shootSound = pygame.mixer.Sound("Assets/Audio/Shoot.mp3")
        shootSound.play()

    def create(self, player):
        self.shootSound()
        x = player.x + player.width / 2
        y = player.y
        self.setPosition(x, y)

    def shouldDestroy(self):
        isCollide = self.isCollidBondsY()
        if isCollide:
            self.alive = False
