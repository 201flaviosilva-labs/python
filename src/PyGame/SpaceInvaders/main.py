import pygame
from math import sqrt, pow

from Bullet import Bullet
from Player import Player
from Enemy import Enemy
from Label import Label

print("https://www.youtube.com/watch?v=FfWpgLFMI7w")

pygame.init()

# Screen
screenWidth = 800
screenHeight = 600
screen = pygame.display.set_mode((screenWidth, screenHeight))

# Backgound and Backgound Sound
# backgroundImg = pygame.image.load("./Assets/Spites/Sky.png")
# pygame.mixer.music.load("background.wav")
# pygame.mixer.music.play(-1)

icon = pygame.image.load("Assets/Spites/InvasorIcon.png")
pygame.display.set_caption("Space Invaders")
pygame.display.set_icon(icon)

# Player
player = Player(screen)
bullet = Bullet(screen)

# Bullet
bullet = Bullet(screen)
bullet.alive = False

# Enemies
numEnemys = 6
enemies = []

# Labels
score = 0
scoreLabel = Label(screen)
scoreTxt = "Score: " + str(score)
scoreLabel.setText(scoreTxt)
scoreLabel.setPosition(0, 20)

for i in range(numEnemys):
    enemy = Enemy(screen)
    enemy.setRandomPosition(screenWidth - enemy.width, 100)
    enemies.append(enemy)


def isCollision(enemy, bullet):
    distance = sqrt(pow(enemy.x - bullet.x, 2) + (pow(enemy.y - bullet.y, 2)))
    return distance < 50


# Draw Screen
def draw():
    screen.fill((122, 122, 122))
    # screen.blit(backgroundImg, (0, 0))

    if player.alive:
        player.update()
    for enemy in enemies:
        if enemy.alive:
            enemy.update()
    if bullet.alive:
        bullet.update()
    scoreLabel.update()

    pygame.display.update()


running = True

while running:
    for event in pygame.event.get():
        # Exit
        if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or event.type == pygame.QUIT:
            running = False

        # Enable Move
        if event.type == pygame.KEYDOWN:  # Key Down
            if event.key == pygame.K_LEFT:
                player.speedX = -player.steepX
            if event.key == pygame.K_RIGHT:
                player.speedX = player.steepX
            if event.key == pygame.K_SPACE and bullet.alive == False:
                bullet.alive = True
                bullet.create(player)

        if event.type == pygame.KEYUP:  # Key Up
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                player.speedX = 0

    # Move
    for enemy in enemies:
        if enemy.isCollidBondsX():
            enemy.speedX = -enemy.speedX
            enemy.setPositionY(enemy.y + enemy.steepY)

    if bullet.alive:
        bullet.shouldDestroy()
        for enemy in enemies:
            if isCollision(enemy, bullet):
                bullet.destroy()
                # enemy.destroy()
                enemy.setRandomPosition(screenWidth - enemy.width, 100)
                score += 1
                scoreLabel.setText("Score: " + str(score))

    for enemy in enemies:
        if enemy.y > screenHeight:
            enemy.setRandomPosition(screenWidth - enemy.width, 100)
            score -= 1
            scoreLabel.setText("Score: " + str(score))

    draw()
