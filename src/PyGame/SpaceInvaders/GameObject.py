import pygame
import math
from random import randint


class GameObject(object):
    def __init__(self, screen):
        self.screen = screen
        self.alive = True
        self.width = 32
        self.height = 32
        self.x = 0
        self.y = 0
        self.speedX = 0
        self.speedY = 0
        self.collideWithWorldBounds = False

    # Position
    def setPosition(self, x, y):
        if self.alive == False:
            return
        self.x = x
        self.y = y

    def setPositionX(self, newX):
        self.setPosition(newX, self.y)

    def setPositionY(self, newY):
        self.setPosition(self.x, newY)

    def setRandomPosition(self):
        x, y = self.screen.get_size()
        newX = randint(0, x - self.width)
        newY = randint(0, y - self.height)
        self.setPosition(newX, newY)

    def setRandomPosition(self, maxX, maxY):
        x, y = self.screen.get_size()
        newX = randint(0, maxX)
        newY = randint(0, maxY)
        self.setPosition(newX, newY)

    # Velocity
    def setVelocityX(self):
        if self.alive == False:
            return
        x, y = self.screen.get_size()
        newX = self.x + self.speedX
        if (newX < 0 or x < newX + self.width) and self.collideWithWorldBounds:
            return
        self.setPositionX(newX)

    def setVelocityY(self):
        if self.alive == False:
            return
        x, y = self.screen.get_size()
        newY = self.y + self.speedY
        if (newY <= 0 or y <= newY + self.height) and self.collideWithWorldBounds:
            return
        self.setPositionY(newY)

    # Check if collide with world/screen bounds
    def isCollidBondsX(self):
        x, y = self.screen.get_size()
        isCollide = False
        if math.floor(self.x) <= 0 or x <= math.ceil(self.x) + self.width:
            isCollide = True
        return isCollide

    def isCollidBondsY(self):
        x, y = self.screen.get_size()
        isCollide = False
        if math.floor(self.y) <= 0 or y <= math.ceil(self.y) + self.height:
            isCollide = True
        return isCollide

    # Update - Draw
    def update(self):
        if not self.alive:
            return
        self.setVelocityX()
        self.setVelocityY()
        self.screen.blit(self.image, (self.x, self.y))

    def destroySound(self):
        shootSound = pygame.mixer.Sound("Assets/Audio/Explosion.mp3")
        shootSound.play()

    def destroy(self):
        self.destroySound()
        self.setPositionY(-1000)
        self.alive = False
