import pygame
from GameObject import GameObject


class Player(GameObject):
    def __init__(self, screen):
        width = 66
        height = 45

        image = pygame.image.load("Assets/Spites/SpaceShip.png")
        image = pygame.transform.scale(image, (width, height))

        x, y = screen.get_size()

        self.alive = True
        self.width = width
        self.height = height
        self.screen = screen
        self.image = image
        self.x = x / 2 - width / 2
        self.y = y - height - 20
        self.speedX = 0  # Actual Speed X
        self.speedY = 0  # Actual Speed Y
        self.collideWithWorldBounds = True

        self.steepX = 0.5  # Speed
