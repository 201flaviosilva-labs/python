import turtle


class Label(turtle.Turtle):
    def __init__(self, *args, **kwargs):
        super(Label, self).__init__(*args, **kwargs)
        self.speed(0)
        self.shape("square")
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(0, 260)

        self.leftScore = 0
        self.rightScore = 0
        self.txt = "Player: {} | CPU: {}".format(self.leftScore, self.rightScore)
        self.write(self.txt, align="center", font=("Courier", 24, "normal"))

    def update(self):
        self.clear()
        self.txt = "Player A: {}  Player B: {}".format(self.leftScore, self.rightScore)
        self.write(self.txt, align="center", font=("Courier", 24, "normal"))
