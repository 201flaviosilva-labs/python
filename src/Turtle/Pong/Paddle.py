import turtle


class Paddle(turtle.Turtle):
    def __init__(self, *args, **kwargs):
        super(Paddle, self).__init__(*args, **kwargs)
        self.speed(0)
        self.shape("square")
        self.color("white")
        self.shapesize(5, 1)
        self.penup()

        self.seed = 1
        self.isMovingUp = False
        self.isMovingDown = False

        self.isBoot = False

    def startBoot(self, ball):
        self.isBoot = True
        self.ball = ball

    def stopBoot(self):
        self.isBoot = False

    def setBoot(self, a, b):
        self.isBoot = not self.isBoot

    def startMoveUp(self):
        # print("Start Up")
        self.isMovingUp = True
        self.isMovingDown = False

    def startMoveDown(self):
        # print("Start Down")
        self.isMovingDown = True
        self.isMovingUp = False

    def stopMoveUp(self):
        print("Stop Down")
        self.isMovingUp = False

    def stopMoveDown(self):
        print("Stop Down")
        self.isMovingDown = False

    def moveUp(self):
        y = self.ycor()
        if y > 300: return
        y += self.seed
        self.sety(y)

    def moveDown(self):
        y = self.ycor()
        if y < -300: return
        y -= self.seed
        self.sety(y)

    def update(self):
        if self.isBoot:  # Boot
            if self.ycor() > self.ball.ycor():
                self.startMoveDown()
            if self.ycor() < self.ball.ycor():
                self.startMoveUp()

        if self.isMovingUp:
            self.moveUp()
        elif self.isMovingDown:
            self.moveDown()
