import turtle
from Paddle import Paddle
from Ball import Ball
from Label import Label


class Main:
    def __init__(self):
        self.width = 800
        self.height = 600

        self.screen = turtle.Screen()
        self.screen.title("Pong")
        self.screen.bgcolor("black")
        self.screen.setup(self.width, self.height)
        self.screen.tracer(0)

        self.running = True

        self.create()
        self.update()
        # End Init

    def create(self):
        self.label = Label()

        self.ball = Ball()
        self.ball.setLabel(self.label)

        # Players
        self.paddleLeft = Paddle()
        self.paddleLeft.goto(-350, 0)
        self.paddleLeft.startBoot(self.ball)

        self.paddleRight = Paddle()
        self.paddleRight.goto(350, 0)
        self.paddleRight.startBoot(self.ball)

        self.addKeyboardListeners()
        # End Create

    def addKeyboardListeners(self):
        self.screen.listen()
        self.screen.onkey(self.paddleLeft.startMoveUp, "w")
        self.screen.onkey(self.paddleLeft.startMoveDown, "s")

        # self.screen.onkeyrelease(self.paddleLeft.stopMoveUp, "w")
        # self.screen.onkeyrelease(self.paddleLeft.stopMoveDown, "s")

        self.screen.onclick(self.paddleLeft.setBoot)

        self.screen.onkeyrelease(self.exit, "q")
        # End keyboardListeners

    def update(self):
        while self.running:
            self.screen.update()

            self.ball.update()
            self.paddleLeft.update()
            self.paddleRight.update()

            # Paddle and ball collisions
            if self.ball.xcor() < -340 and self.paddleLeft.ycor() + 50 > self.ball.ycor() > self.paddleLeft.ycor() - 50:
                self.ball.directionX *= -1

            elif self.ball.xcor() > 340 and self.paddleRight.ycor() + 50 > self.ball.ycor() > self.paddleRight.ycor() - 50:
                self.ball.directionX *= -1

            # End while
        # End update

    def exit(self):
        self.running = False


# End class Main


game = Main()
