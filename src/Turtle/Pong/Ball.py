import turtle


class Ball(turtle.Turtle):
    def __init__(self, *args, **kwargs):
        super(Ball, self).__init__(*args, **kwargs)
        self.speed(0)
        self.shape("circle")
        self.color("white")
        self.shapesize(1)
        self.penup()
        self.goto(0, 0)

        self.directionX = 1
        self.directionY = 1

    def setLabel(self, label):
        self.label = label

    def update(self):
        # X
        if self.xcor() >= 400:
            self.directionX *= -1
            self.label.leftScore += 1
            self.label.update()
        if self.xcor() <= -400:
            self.directionX *= -1
            self.label.rightScore += 1
            self.label.update()

        # Y
        if self.ycor() >= 300 or self.ycor() <= -300:
            self.directionY *= -1

        # Move
        self.setx(self.xcor() + self.directionX)
        self.sety(self.ycor() + self.directionY)
