# Game -> https://codepen.io/zentech/full/XaYygR -> animation time: 0.5s
# Tutorial: https://www.youtube.com/watch?v=roW2Fu4IjeA

# Green -> (0 , 170, 0) - (1450, 350)
# Red -> (255, 0, 0) - (1750, 350)
# Blue -> (0, 0, 255) - (1750, 650)
# Yellow -> (255, 255, 0) - (1450, 650)

import pyautogui
from time import sleep
import keyboard

# Normal Colors
green_color = (0, 170, 0)
red_color = (255, 0, 0)
blue_color = (0, 0, 255)
yellow_color = (255, 255, 0)

# Positions
green_position = (1450, 350)
red_position = (1750, 350)
blue_position = (1750, 650)
yellow_position = (1450, 650)

# --
color_sequence = []
score = 1
current_color = 0
is_listening = True
delayTime = 0.3


def get_color_changed():
    # -- GREEN
    if pyautogui.pixel(green_position[0], green_position[1]) != green_color:
        print("Getted -> Green")
        color_sequence.append(green_position)
        # White the reset of the color
        while pyautogui.pixel(green_position[0], green_position[1]) != green_color:
            sleep(0.01)
    # -- RED
    if pyautogui.pixel(red_position[0], red_position[1]) != red_color:
        print("Getted -> Red")
        color_sequence.append(red_position)
        while pyautogui.pixel(red_position[0], red_position[1]) != red_color:
            sleep(0.01)

    # -- BLUE
    if pyautogui.pixel(blue_position[0], blue_position[1]) != blue_color:
        print("Getted -> Blue")
        color_sequence.append(blue_position)
        while pyautogui.pixel(blue_position[0], blue_position[1]) != blue_color:
            sleep(0.01)

    # -- YELLOW
    if pyautogui.pixel(yellow_position[0], yellow_position[1]) != yellow_color:
        print("Getted -> Yellow")
        color_sequence.append(yellow_position)
        while pyautogui.pixel(yellow_position[0], yellow_position[1]) != yellow_color:
            sleep(0.01)


def click_sequence():
    pyautogui.click(color_sequence[current_color]
                    [0], color_sequence[current_color][1])

    # print color clicked
    if color_sequence[current_color] == green_position:
        print("Clicked -> Green")
    elif color_sequence[current_color] == red_position:
        print("Clicked -> Red")
    elif color_sequence[current_color] == blue_position:
        print("Clicked -> Blue")
    elif color_sequence[current_color] == yellow_position:
        print("Clicked -> Yellow")

    pass


def main():
    global is_listening
    global score
    global current_color
    global color_sequence
    global delayTime

    while True:
        sleep(delayTime)
        if keyboard.is_pressed("space"):
            break  # Stop the loop
            pass

        # Game loop
        if is_listening:
            get_color_changed()
            if score == len(color_sequence):
                is_listening = False
                print()
                print("------")
                print("-- Listening is off: ")
        else:
            click_sequence()
            current_color += 1
            if current_color == len(color_sequence):
                color_sequence = []
                current_color = 0
                score += 1
                is_listening = True
                print("Score: " + str(score))
                print("------")
                print("-- Listening is on:")
                sleep(0.2)

            pass

        pass


if __name__ == "__main__":
    main()

print("--- End ---")
