# Game: https://codepen.io/201flaviosilva/pen/PoQzbod
# Tutorial: https://www.youtube.com/watch?v=TERKvqfySYI

import pyautogui
import keyboard
import win32api
import win32con
from time import sleep

sleep(4)

start_x = 750
start_y = 350
end_x = 850
end_y = 650


def click(x, y):
    position = (x, y)
    win32api.SetCursorPos(position)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    sleep(.01)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    pass


# sc = pyautogui.screenshot(region=(start_x, start_y, end_x, end_y))
# sc.save("image.png")

while True:
    sleep(0.1)
    if keyboard.is_pressed("space"):
        quit()
        break

    sc = pyautogui.screenshot(region=(start_x, start_y, end_x, end_y))
    width, height = sc.size

    for x in range(0, width, 5):
        isTargetFounded = False
        for y in range(0, height, 5):
            r, g, b = sc.getpixel((x, y))

            if r > 240 and g < 10 and b < 10:
                # print(r, g, b, x, y)
                isTargetFounded = True
                click(start_x + x, start_y + y)
                break

        if isTargetFounded:
            break
