# Tutorial: https://www.youtube.com/watch?v=YRAIUA-Oc1Y
# Game: https://www.agame.com/game/magic-piano-tiles

import pyautogui
from time import sleep
import keyboard
import win32api
import win32con

tile1 = (1450, 500)
tile2 = (1540, 500)
tile3 = (1630, 500)
tile4 = (1720, 500)

tiles = [tile1, tile2, tile3, tile4]

sleep(2)


def click(x, y):
    win32api.SetCursorPos((x, y))
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    sleep(0.1)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)


while keyboard.is_pressed("space") == False:
    for tile in tiles:
        if pyautogui.pixel(tile[0], tile[1])[0] == 0:
            click(tile[0], tile[1])
