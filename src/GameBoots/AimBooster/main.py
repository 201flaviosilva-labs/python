# Tutorial: https://www.youtube.com/watch?v=YRAIUA-Oc1Y
# Game: http://www.aimbooster.com/

import pyautogui
from time import sleep
import keyboard
import win32api
import win32con

sleep(2)

screen_cords = (1250, 350, 650, 450)
target_color = (255, 219, 195)


def click(x, y):
    win32api.SetCursorPos((x, y))
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    sleep(0.05)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)


while keyboard.is_pressed("space") == False:
    flag = 0
    pic = pyautogui.screenshot(region=screen_cords)

    width, height = pic.size

    for x in range(0, width, 5):
        for y in range(0, height, 5):

            r, g, b = pic.getpixel((x, y))

            if r == target_color[0] and g == target_color[1] and b == target_color[2]:
                flag = 1
                click(x + screen_cords[0], y + screen_cords[1])
                sleep(0.05)
                break

        if flag == 1:
            break
