# Game: https://www.safekidgames.com/spot-the-difference/

import pyautogui
import keyboard
import win32api
import win32con
from time import sleep
from PIL import Image, ImageChops

# sleep(2)

start_y = 739
start_original = (1277, start_y)
start_diff = (1577, start_y)
image_size = (270, 158)
founds_positions = []

# Original
sc_original = pyautogui.screenshot(
    region=(start_original[0], start_original[1], image_size[0], image_size[1]))
sc_original.save("original.png")

# Different
sc_diff = pyautogui.screenshot(
    region=(start_diff[0], start_diff[1], image_size[0], image_size[1]))
sc_diff.save("different.png")


def compare_images(orig_img_src, diff_img_src):
    # assign images
    orig_img = Image.open(orig_img_src)
    diff_img = Image.open(diff_img_src)

    # finding difference
    diff_img = ImageChops.difference(orig_img, diff_img)

    # find the position of the difference
    diff_img.save("diff.png")
    diff_img = Image.open("diff.png")
    diff_img.load()
    data = diff_img.getdata()

    for pixel in data:
        if pixel != (0, 0, 0, 255):
            has_been_found = False
            for found in founds_positions:
                if found == pixel:
                    has_been_found = True
                    break

            if not has_been_found:
                founds_positions.append(pixel)
                position = (pixel[0], pixel[1])
                return position

    # showing the difference
    # diff_img.show()


def click(x, y):
    position = (x, y)
    win32api.SetCursorPos(position)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    sleep(.01)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    pass


while True:
    sleep(0.25)
    if keyboard.is_pressed("space"):
        quit()
        break

    # compare images
    diff_pos = compare_images("original.png", "different.png")
    print(diff_pos)

    # click on the difference
    click(start_original[0] + diff_pos[0], start_original[1] + diff_pos[1])
    pass
