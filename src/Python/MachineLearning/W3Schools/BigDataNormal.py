import numpy
import matplotlib.pyplot as plt

nums = numpy.random.normal(5.0, 1.0, 100000)

plt.hist(nums, 100)
plt.show()
