import numpy
import matplotlib.pyplot as plt

numbers = numpy.random.uniform(0.0, 5.0, 100000)
plt.hist(numbers, 100)
plt.title("Uniform")
plt.show()
