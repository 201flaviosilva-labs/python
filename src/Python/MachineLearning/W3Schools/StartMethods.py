import numpy
from scipy import stats
from random import randint

print()
print("Média, Mediana, Moda ➜ https://www.w3schools.com/python/python_ml_mean_median_mode.asp")
print()

maxSize = 25
numbers = []

for i in range(maxSize):
    n = randint(0, 100)
    numbers.append(n)

mean = numpy.mean(numbers)
median = numpy.median(numbers)
mode = stats.mode(numbers)
standardDeviation = numpy.std(numbers)
variance = numpy.var(numbers)
percentile = numpy.percentile(numbers, 25)

print("Números: {}".format(numbers))
print("Média: {}".format(mean))
print("Mediana: {}".format(median))
print("Moda: {}".format(mode))
print("Desvio padrão: {}".format(standardDeviation))
print("Variância: {}".format(standardDeviation))
print("Percentile de 25: {} %".format(percentile))
