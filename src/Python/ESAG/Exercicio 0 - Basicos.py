# olá Mundo
print('Ola Mundo')
print('')

# contas
print(2 + 3)
print(10 / 2)
print(2 ** 5)
print('')

# Mais Contas
print("2" + "3")
print(int("2") + int("3"))
print("Spam" + "Ovos")
print("Spam" + "2")
print('Spam' * 2)
print("Beep " * 10)
print('')

# inputs
nome1 = input("Diga o teu primeiro nome: ")
print(nome1 + " " + input("Diga o teu ultimo nome: "))
print('')

# Comentario
"""comentário
mais
que
uma
linha"""

# Ciclos
i = 1
x = 1
while i <= 5:
    i = i+1
    x = x*i
    print(x)
print('')

# Ifs
y = 4
z = 10
if y + z == 15:
    print("V")
else:
    print("F")
print('')
