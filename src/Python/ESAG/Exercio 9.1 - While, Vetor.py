print('Ciclo While, vetores')

resposta = 's'
fatura = []
Total = 0
valid_preco = False


while resposta == 's':
    print('')
    produto = input('Digite o nome do produto: ')
    while valid_preco == False:
        preco = float(input('Digite o preço do produto: '))
        try:
            preco = float(preco)
            if preco <= 0:
                print('O preço tem de ser maior que zero')
            else:
                valid_preco = True
        except:
            print: ('Formato de preço inválido.')

    fatura.append([produto, preco])

    Total += preco
    valid_preco = False
    resposta = input('Queres continuar?')


for i in fatura:
    print(i[0], ' - ', i[1])

print('O total da fatura é: ', Total)
