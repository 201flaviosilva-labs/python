print('Ciclo for')
numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

num = int(input('Qual é o número: '))

for n in numeros:
    print(n, ' X ', num, ' = ', n*num)

print('')

nomes = ['Pedro', 'Maria', 'João']

for x in nomes:
    print(x)
