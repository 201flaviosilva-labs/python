import math
from math import cos, sin, tan

print("https://www.youtube.com/watch?v=oOUyhGNib2Q&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=24")
print("A008a ➜ Utilizando Módulos")

num1 = 5
num2 = 2
numPi = math.pi

squareRoot = math.sqrt(num1)
roundCeil = math.ceil(squareRoot)
roundFloor = math.floor(squareRoot)
trunc = math.trunc(squareRoot)
po = math.pow(num1, num2)
fact = math.factorial(num1)

cosine = cos(num1)
sine = sin(num1)
tangent = tan(num1)

print("PI π: math.pi = {} ".format(numPi))
print("Raiz Quadrada: √{} = {} ".format(num1, squareRoot))
print("Arredondar 2 casas decimais: √{} = {:.2f} ".format(num1, squareRoot))
print("Arredondar para Cima: √{} = {}".format(num1, roundCeil))
print("Arredondar para Baixo: √{} = {}".format(num1, roundFloor))
print("Remover números a partir da virgula: √{} = {}".format(num1, trunc))
print("Potencia: math.pow({}, {}) = {}".format(num1, num2, po))
print("Fatorial: math.factorial({}) = {}".format(num1, fact))

print("Cosseno: cos({}) = {}".format(num1, cosine))
print("Seno: sin({}) = {}".format(num1, sine))
print("Tangente: tan({}) = {}".format(num1, tangent))
