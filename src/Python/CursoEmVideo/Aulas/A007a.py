print("https://www.youtube.com/watch?v=Vw6gLypRKmY&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6")
print("A007a ➜ Operadores Aritméticos")

num1 = int(input("Número 1: "))
num2 = int(input("Número 2: "))

sum = num1 + num2
sub = num1 - num2
mult = num1 * num2
div = num1 / num2
rest = num1 % num2
po = num1 ** num2
po2 = pow(num1, num2)
intDiv = num1 // num2

print("Soma: {} + {} = {} ".format(num1, num2, sum))
print("Subtração: {} - {} = {} ".format(num1, num2, sub))
print("Multiplicação: {} * {} = {} ".format(num1, num2, mult))
print("Divisão: {} / {} = {} ".format(num1, num2, div))
print("Resto: {} % {} = {} ".format(num1, num2, rest))
print("Potência: {} ** {} = {} ".format(num1, num2, po))
print("Potência 2: pow({}, {}) = {} ".format(num1, num2, po2))
print("Dvisisão inteira: {} // {} = {} ".format(num1, num2, intDiv))
