print("https://www.youtube.com/watch?v=a7DH88vk2Sk&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=31")
print("A009a ➜ Manipulando Texto")

sentence1 = "Curso em Video Python (Fateamento/Análise)"
sentence2 = "  Curso em    Video   Python (Tranformação)  "

sentenceReplace = sentence2.replace("Python", "JavaScript")
sentenceUpper = sentence2.upper()
sentenceLower = sentence2.lower()
sentenceCapitalize = sentence2.capitalize()
sentenceTitle = sentence2.title()
sentenceStrip = sentence2.strip()
sentenceRStrip = sentence2.rstrip()
sentenceLStrip = sentence2.lstrip()

sentenceSplit = sentence1.split()
sentenceJoin = "-".join(sentenceSplit)

# Fateamento
print("-- Fateamento")
print(sentence1)
print("sentence[9] = {}".format(sentence1[9]))
print("sentence[9:14] = {}".format(sentence1[9:14]))
print("sentence[9:21:2] = {}".format(sentence1[9:21:2]))
print("sentence[:5] = {}".format(sentence1[:5]))
print("sentence[15:] = {}".format(sentence1[15:]))
print("sentence[9::3] = {}".format(sentence1[9::3]))
print("sentence[::5] = {}".format(sentence1[::5]))
print("")

# Análise
print("-- Análise")
print("len(sentence) = {}".format(len(sentence1)))
print("sentence.count('o') = {}".format(sentence1.count('o')))
print("sentence.count('o', 0, 13) = {}".format(sentence1.count('o', 0, 13)))
print("sentence.find('Python') = {}".format(sentence1.find('Python')))
print("sentence.find('JavaScript') = {}".format(sentence1.find('JavaScript')))
print("'Python' in sentence = {}".format('Python' in sentence1))
print("'JavaScript' in sentence = {}".format('JavaScript' in sentence1))
print("")

# Tranformação
print("-- Tranformação")
print(sentence2)
print("sentence2.replace('Python', 'JavaScript') = {}".format(sentenceReplace))
print("sentence2.upper() = {}".format(sentenceUpper))
print("sentence2.lower() = {}".format(sentenceLower))
print("sentence2.capitalize() = {}".format(sentenceCapitalize))
print("sentence2.title() = {}".format(sentenceTitle))
print("sentence2.strip() = {}".format(sentenceStrip))
print("sentence2.rstrip() = {}".format(sentenceRStrip))
print("sentence2.lstrip() = {}".format(sentenceLStrip))
print("")

# Divisão
print("-- Divisão")
print(sentence1)
print("sentence.split() = {}".format(sentenceSplit))
print("-- Junção")
print("'-'.join(sentenceSplit) = {}".format(sentenceJoin))
