print("https://www.youtube.com/watch?v=a7DH88vk2Sk&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=31")
print("A009b ➜ Manipulando Texto")

sentence = """
Aquela cativa
Que me tem cativo,
Porque nela vivo
Já não quer que viva.
Eu nunca vi rosa
Em suaves molhos,
Que pera meus olhos
Fosse mais fermosa.

        Luís de Camões
"""

print(sentence)
