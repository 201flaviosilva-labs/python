print("https://www.youtube.com/watch?v=Vw6gLypRKmY&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6")
print("A007b ➜ Operadores Aritméticos Em Strings")

string = str(input("String: "))
num = 10

mult = string * num

print("Multiplicação: {} * {} = {} ".format(string, num, mult))
print(": {:20} com espaço! (Predefenido)".format(string))
print(":> {:>20} com espaço! (Direita)".format(string))
print(":< {:<20} com espaç! (Esquerda)".format(string))
print(":^ {:^20} com espaço! (Centro)".format(string))
print(":=^ {:=^20} com espaço! (Centro com caracteres)".format(string))
