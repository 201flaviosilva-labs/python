import random

print("https://www.youtube.com/watch?v=oOUyhGNib2Q&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=24")
print("A008a ➜ Utilizando Módulos (Random)")

numRandom = random.random()
numRandomInt = random.randint(1, 100)

print("Random: {}".format(numRandom))
print("Random entre (1, 100): {}".format(numRandomInt))
