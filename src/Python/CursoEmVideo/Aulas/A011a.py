import math

print("https://www.youtube.com/watch?v=0hBIhkcA8O8&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=47")
print("A011a ➜  Cores no Terminal")
print()

print("8:40 ➜ Quadro com as cores do terminal")

print("\033[0:30:44m Beep")
print("\033[1:33:42m Beep\033[m")
print("\033[4:35:41m Beep")
print("Beep")
print("\033[7:37:46m Beep\033[m")
print("\033[34:43m Beep")
print("\033[m Beep")

colors = {
    "clear": "\033[m",
    "subRedBlue": "\033[4:35:44m",
    "boldGreenBlack": "\033[1:33:40m",
    "blue": "\033[0:34m",
}

print("")
print("-=-" * 20)
print("{} Beep {}".format(colors["subRedBlue"], colors["clear"]))
print("{} Beep {}".format(colors["boldGreenBlack"], colors["clear"]))
print("{} Beep {}".format(colors["blue"], colors["clear"]))
print("Beep")
print("")
print("")
print("----- Fim Mundo 1 -----")
