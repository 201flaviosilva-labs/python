from random import randint

print("https://www.youtube.com/watch?v=K10u3XIf1-Q&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=38")
print("A010a ➜  Condições (Parte 1)")

num = randint(0, 10)
print(num)

# V1
if num <= 4:
    print("Carro Novinho em folha")
else:
    print("Bem velho o carro")

# V2
print("----")

print("Novo" if num <= 4 else "Velho")
