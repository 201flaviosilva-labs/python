from random import randint

print("https://www.youtube.com/watch?v=j9bYDjaAYzw&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=2")
print("A012a ➜  Condições Aninhadas")
print()

num = randint(0, 10)

if num >= 7:
    print("Número Grande")
elif num < 7 and num > 3:
    print("Número Médio")
else:
    print("Número Baixo")

print("Número: {}".format(num))
