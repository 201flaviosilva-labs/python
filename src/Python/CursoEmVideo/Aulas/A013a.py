from random import randint

print("https://www.youtube.com/watch?v=cL4YDtFnCt4&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=13")
print("A013a ➜ Estrutura de repetição for")
print()

print("for i in range(0, 100, 7): // começa no 0, acaba no 100, saltando de 7 em 7")
for i in range(0, 100, 7):
    print("i: {}".format(i))

print("-" * 100)
print("For reverso")
print("for i in range(10, -5, -1):")
for i in range(10, -5, -1):
    print("i: {}".format(i))
