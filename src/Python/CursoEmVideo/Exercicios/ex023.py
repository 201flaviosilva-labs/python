# :,)

from random import randint

print("https://www.youtube.com/watch?v=wD2aerLMBWA&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=33")
print("EX023 ➜ Separando dígitos de um número")
print()

num = randint(1, 999)
# distance = "1834"
# distance = "436"
# distance = "27"
# distance = "7"

u = int(num) // 1 % 10
d = int(num) // 10 % 10
c = int(num) // 100 % 10
m = int(num) // 1000 % 10

# print("Número {}: Uni:({}), Des:({}), Cen:({}), Mil:({})".format(distance, distance[3], distance[2], distance[1], distance[0]))

# Solução :)
print("Número {}: Uni:({}), Des:({}), Cen:({}), Mil:({})".format(num, u, d, c, m))
