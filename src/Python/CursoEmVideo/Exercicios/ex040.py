from random import randint

print("https://www.youtube.com/watch?v=QuWDyUeoaJs&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=7")
print("EX040 ➜ Aquele clássico da Média")
print("")

num1 = randint(0, 10)
print("Nota: {}".format(num1))

if num1 < 5:
    print("Reprovado! :(")
elif num1 >= 5 and num1 < 7:
    print("Recuperação :0")
else:
    print("Passou :)")
