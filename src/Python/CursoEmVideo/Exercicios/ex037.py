from random import randint

print("https://www.youtube.com/watch?v=B3F0IjH5WAM&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=4")
print("EX037 ➜ Conversor de Bases Numéricas")
print("")

num1 = randint(0, 1000)
convertionType = randint(1, 3)

if convertionType == 1:
    print("Binário: {}".format(bin(num1)))
elif convertionType == 2:
    print("Octa: {}".format(oct(num1)))
else:
    print("Hexadécimal: {}".format(hex(num1)))
