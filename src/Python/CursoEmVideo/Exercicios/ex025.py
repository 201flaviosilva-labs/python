print("https://www.youtube.com/watch?v=WHWGz2Dy1ZU&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=35")
print("EX025 ➜ Procurando uma string dentro de outra")
print()

name1 = "Flávio Silva"
name2 = "Aníbal Augusto Milhais (Soldado Milhões)"

containName = "Silva"

print("{} tem {}? : {}".format(name1, containName, containName.upper() in name1.upper()))
print("{} tem {}? : {}".format(name2, containName, containName.upper() in name2.upper()))
