from random import choice

print("https://www.youtube.com/watch?v=tapTa6KVG-A&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=12")
print("EX045 ➜ GAME: Pedra Papel e Tesoura")
print("")

options = ["pedra", "papel", "tesoura"]

# playerChoise = str(input("Pedra/Papel/Tesoura: ")).lower().strip()

playerChoise = choice(options)
CPUChoise = choice(options)

print("Player: {}".format(playerChoise))
print("CPU: {}".format(CPUChoise))

if playerChoise == CPUChoise:
    print("Empate!")
elif (playerChoise == "pedra" and CPUChoise == "papel") \
        or (playerChoise == "papel" and CPUChoise == "tesoura") \
        or (playerChoise == "tesoura" and CPUChoise == "pedra"):
    print("Perdeste :(")
else:
    print("Venceste :)")
