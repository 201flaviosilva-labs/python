print("https://www.youtube.com/watch?v=xM4AX3Lp2mo&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=18")
print("EX010 ➜ Conversor de Moedas")

num = float(input("Número ➜ "))

# Valores ➜ 26/06/2021
dollar = 1.19  # 1€ == 1.19$
pound = 0.86  # 1€ == 0.86 £
realBR = 5.89  # 1€ == 5.89 R$

print("Dólar ➜ {} * {} = {}".format(num, dollar, num * dollar))
print("Libra (Pound) ➜ {} * {} = {}".format(num, pound, num * pound))
print("Real Brasileiro ➜ {} * {} = {}".format(num, realBR, num * realBR))
