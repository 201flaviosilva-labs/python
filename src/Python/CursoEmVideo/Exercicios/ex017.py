from math import hypot
from random import randint

print("https://www.youtube.com/watch?v=vmPW9iWsYkY&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=26")
print("EX017 ➜ Catetos e Hipotenusa")

min = 5
max = 100

num1 = randint(min, max)
num2 = randint(min, max)
h1 = (num1 ** 2 + num2 ** 2) ** (1 / 2)
h2 = hypot(num1, num2)

print("Teorema de Pitagoras: ({} ** 2 + {} ** 2) ** (1/2) = {}".format(num1, num2, h1))
print("hypot({}, {}) = {}".format(num1, num2, h2))
