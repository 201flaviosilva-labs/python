from random import randint

print("https://www.youtube.com/watch?v=-OnqSGh0u4g&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=19")
print("EX051 ➜ Progressão Aritmética")

start = randint(0, 10)
end = randint(1, 10) * start
steep = randint(1, 10)

print("Inicio: {}".format(start))
print("Fim: {}".format(end))
print("Salto: {}".format(steep))

for i in range(start, end, steep):
    print("{}".format(i))
