print("https://www.youtube.com/watch?v=mzSJpn9ldt4&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=19")
print("EX011 ➜ Pintando Parede")

num1 = float(input("Largura (Num) ➜ "))
num2 = float(input("Altura (Num) ➜ "))
tint = 2  # Cada metro de area para pintar
area = num1 * num2
tintForPaint = (area * 1) / tint  # Regra de 3 simples

print("Litros por metro: {}".format(tint))
print("Area: {} * {} = {}m²".format(num1, num2, area))
print("Litros de tinta: {} ** {} = {}".format(area, tint, tintForPaint))
