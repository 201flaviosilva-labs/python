from random import randint

print("https://www.youtube.com/watch?v=4vFCzKuHOn4&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=41")
print("EX030 ➜ Par ou Ímpar?")
print("")

num = randint(0, 120)

print("Número: {}".format(num))
print("Par" if num % 2 == 0 else "Ímpar")
