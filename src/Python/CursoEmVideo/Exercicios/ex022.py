print("https://www.youtube.com/watch?v=EQQt-6QqXOs&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=32")
print("EX022 ➜ Analisador de Textos")

name = "Luís Vaz de Camões"

print("Nome original: {}".format(name))
print("Upper: {}".format(name.upper()))
print("Lower: {}".format(name.lower()))
print("Count: {}".format(len(name) - name.count(" ")))
print("Nº Letras Primeiro nome: {}".format(len(name.split()[0])))
