from random import randint

print("https://www.youtube.com/watch?v=PGqHyzWoagc&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=42")
print("EX031 ➜ Custo da Viagem")
print("")

distance = randint(1, 400)
maxKM = 200
price = 0

if distance <= maxKM:
    price = distance * 0.5
else:
    price = distance * 0.45

print("{} km".format(distance))
print("Valor a pagar: {}€".format(price))
