from random import randint

print("https://www.youtube.com/watch?v=a_8FbW5oH6I&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=44")
print("EX033 ➜ Maior e menor valores")
print("")

num1 = randint(0, 1000)
num2 = randint(0, 1000)
num3 = randint(0, 1000)

numsSorted = [num1, num2, num3]
numsSorted.sort()  # Bom, não foi feito com condições, mas serve :)

print("Numeros: {}, {}, {}".format(num1, num2, num3))

print("Maior: {}".format(numsSorted[2]))
print("Menor: {}".format(numsSorted[0]))
