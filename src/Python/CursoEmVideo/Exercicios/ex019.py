from random import randint, choice

print("https://www.youtube.com/watch?v=_Nk02-mfB5I&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=28")
print("EX019 ➜ Sorteando um item na lista")

names = ["Luís Vaz de Camões", "Zé Povinho", "Afonso de Albuquerque", "Rainha Santa Isabel"]
print(names)

# Forma 1 :)
randomName1 = names[randint(0, len(names) - 1)]
print("Nome Escolido: {}".format(randomName1))

# Forma 2 (Exemplo do video)
randomName2 = choice(names)
print("Nome Escolido choice(): {}".format(randomName2))
