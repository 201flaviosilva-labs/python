print("https://www.youtube.com/watch?v=iHjsUxNA-wo&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=16")
print("EX048 ➜ Soma ímpares múltiplos de três")
print("")

sum = 0
for i in range(1, 500):
    if i % 2 == 1 and i % 3 == 0:
        sum += i

print(sum)
