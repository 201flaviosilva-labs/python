print("https://www.youtube.com/watch?v=5VBWe6BXzRo&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=21")
print("EX053 ➜ Detector de Palíndromo")
print()

txt = "Apos a sopa"
# txt = "As coisas que vi"

print("Frase: {}".format(txt))

reverseTxt = ""
txt = txt.upper().split()
txt = "".join(txt)

for i in range(len(txt) - 1, -1, -1):
    reverseTxt += txt[i]

if reverseTxt == txt:
    print("É um Palíndromo")
else:
    print("Nop")
