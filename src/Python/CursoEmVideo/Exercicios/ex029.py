from random import randint

print("https://www.youtube.com/watch?v=hgJ_ETNGSj8&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=40")
print("EX029 ➜ Radar eletrônico")
print("")

carSpeed = randint(0, 120)
maxSpeed = 80

price = 7
trafficTicket = (carSpeed - maxSpeed) * price

print("Velocidade {} / {} max".format(carSpeed, maxSpeed))

if carSpeed > maxSpeed:
    print("Olha a multa: {}€".format(trafficTicket))
else:
    print("É está tudo ok, podes ir ainda mais rápido :)")
