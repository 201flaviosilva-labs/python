from random import randint

print("https://www.youtube.com/watch?v=IL5iBWoKRIs&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=22")
print("EX054 ➜ Grupo da Maioridade")
print()

sumAdult = 0
numPersons = 10

for i in range(numPersons):
    age = randint(1, 50)
    print("Idade {}: {}".format(i + 1, age))
    if age >= 18: sumAdult += 1

print()
print("De {} pessoas".format(numPersons))
print("Adultos: {}".format(sumAdult))
print("Crianças: {}".format(numPersons - sumAdult))
