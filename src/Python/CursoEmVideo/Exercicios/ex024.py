print("https://www.youtube.com/watch?v=QroT8cZMRnc&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=34")
print("EX024 ➜ Verificando as primeiras letras de um texto")
print()

city1 = "Santo Tirso"
city2 = "Chão de Couce dos Santos"

print("Cidade 1: {}".format(city1))
print("Cidade 2: {}".format(city2))

print("Cidade 1 contem Santo no inicio? : {}".format(city1.upper().split()[0] == "Santo".upper()))
print("Cidade 2 contem Santo no inicio? : {}".format(city2.upper().split()[0] == "Santo".upper()))
