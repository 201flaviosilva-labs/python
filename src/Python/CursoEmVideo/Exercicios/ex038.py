from random import randint

print("https://www.youtube.com/watch?v=iuPbB9uHczM&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=5")
print("EX038 ➜ Comparando números")
print("")

num1 = randint(0, 10)
num2 = randint(0, 10)

if num1 > num2:
    print("{} > {}".format(num1, num2))
elif num1 < num2:
    print("{} < {}".format(num1, num2))
else:
    print("{} == {}".format(num1, num2))
