from random import randint

print("https://www.youtube.com/watch?v=kchC5KLZSZ4&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=39")
print("EX028 ➜ Jogo da Adivinhação v.1.0")
print("")

hideNumber = randint(0, 5)
attemptNumber = randint(0, 5)

print("Número Escondido: {}".format(hideNumber))
print("Número tentado: {}".format(attemptNumber))

if hideNumber == attemptNumber:
    print("O Python acertou no número que o Python escondeu :)")
else:
    print("O Python falhou no número que o Python escondeu :(")
