from math import cos, sin, tan, radians
from random import randint

print("https://www.youtube.com/watch?v=9GvsphwW26k&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=27")
print("EX018 ➜ Seno, Cosseno e Tangente")

min = 0
max = 360

num1 = randint(min, max)
num1 = 45

cosineRad = radians(num1)
sineRad = radians(num1)
tangentRad = radians(num1)

cosine = cos(cosineRad)
sine = sin(sineRad)
tangent = tan(tangentRad)

print("Cosseno: radians({}) = cos({}) = {}".format(num1, cosineRad, cosine))
print("Seno: radians({}) = sin({}) = {}".format(num1, sineRad, sine))
print("Tangente: radians({}) = tan({}) = {}".format(num1, tangentRad, tangent))
