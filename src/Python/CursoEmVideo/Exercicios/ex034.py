from random import randint

print("https://www.youtube.com/watch?v=Sfadj_AzKHw&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=45")
print("EX034 ➜ Aumentos múltiplos")
print("")

salary = randint(500, 2000)
maxSal = 1250
increase = 0

print("Salário: {}".format(salary))

if salary > maxSal:
    increase = salary * 0.1
else:
    increase = salary * 0.15

salary += increase
print("Aumento: {}".format(salary))
