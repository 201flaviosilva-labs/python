print("https://www.youtube.com/watch?v=23UOVEetNPY&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=36")
print("EX026 ➜ Primeira e última ocorrência de uma string")
print()

sentence = "Cada vilão é o herói da sua própria história;"
char = "a"

sentenceUpper = sentence.upper()
charUpper = char.upper()

print("Frase original: {}".format(sentence))
print("Contar {} = {}".format(char, sentenceUpper.count(charUpper)))
print("Primeiro {} = {}".format(char, sentenceUpper.find(charUpper)))
print("Primeiro {} = {}".format(char, sentenceUpper.rfind(charUpper)))
