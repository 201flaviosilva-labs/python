from math import trunc
from random import uniform

print("https://www.youtube.com/watch?v=-iSbDpl5Jhw&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=25")
print("EX016 ➜ Quebrando um número")

min = -100
max = 100

num1 = uniform(min, max)
num2 = uniform(min, max)
num3 = uniform(min, max)
num4 = uniform(min, max)

print("trunc({}) = {}".format(num1, trunc(num1)))
print("trunc({}) = {}".format(num2, trunc(num2)))
print("trunc({}) = {}".format(num3, trunc(num3)))
print("trunc({}) = {}".format(num4, trunc(num4)))
