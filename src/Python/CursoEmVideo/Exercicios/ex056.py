from random import choice, randint

print("https://www.youtube.com/watch?v=fokDF4th0IY&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=24")
print("EX056 ➜ Analisador completo")
print()

numPersons = 4

ageAverage = 0
older = 0
olderName = 0
femaleSumAge = 0  # women over 20 years old

maleNames = ["São Pedro", "João", "Vasco da Gama", "Nando Pessoa", "Dº Nuno Alveres Pereira"]
femaleNames = ["Rainha Santa Isabel", "Inês de Castro", "Brites de Almeida (Padeira de Aljubarrota)", "Beatriz Costa"]
sexList = ["M", "F"]

for i in range(numPersons):
    sex = choice(sexList)
    name = ""
    age = randint(0, 50)

    ageAverage += age
    if sex == "M":
        name = choice(maleNames)
        if older < age: olderName = name
    else:
        name = choice(femaleNames)
        if age >= 18: femaleSumAge += 1

    print("Nome: {}, Idade: {}, Sexo: {}".format(name, age, sex))

print()
print("Média: {}".format(ageAverage / numPersons))
print("Homem mais velho: {}".format(olderName))
print("Nº mulheres maiores de idade: {}".format(femaleSumAge))
