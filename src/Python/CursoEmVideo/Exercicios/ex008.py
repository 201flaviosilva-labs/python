print("https://www.youtube.com/watch?v=KjcdG05EAZc&list=PLHz_AreHm4dlKP6QQCekuIPky1CiwmdI6&index=16")
print("EX008 ➜ Conversor de Medidas")

num = float(input("Número (Metros) ➜ "))

print("Quilômetro ➜ {}".format(num * 0.001))
print("Hectômetro ➜ {}".format(num * 0.01))
print("Decâmetro ➜ {}".format(num * 0.1))
print("Métro ➜ {}".format(num))
print("Decímetro ➜ {}".format(num * 10))
print("Centímetro ➜ {}".format(num * 100))
print("Milímetro ➜ {}".format(num * 1000))
