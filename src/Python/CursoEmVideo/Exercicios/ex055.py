from random import randint

print("https://www.youtube.com/watch?v=Kjpb_IAOKRQ&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=23")
print("EX055 ➜ Maior e menor da sequência")
print()

heavier = 0
lighter = 100000
numPersons = 5

for i in range(numPersons):
    weight = randint(0, 100)
    print("Idade {}: {}".format(i + 1, weight))
    if weight > heavier: heavier = weight
    if weight < lighter: lighter = weight

print()
print("Mais pesado: {}".format(heavier))
print("Mais leve: {}".format(lighter))
