from random import uniform

print("https://www.youtube.com/watch?v=b7r34za963I&list=PLHz_AreHm4dk_nZHmxxf_J0WRAqy5Czye&index=10")
print("EX043 ➜ Índice de Massa Corporal")
print("")

weight = uniform(30, 100)
height = uniform(1.5, 2)
imc = weight / (height ** 2)

print("Peso: {}".format(weight))
print("Altura: {}".format(height))

print("IMC: {}".format(imc))
print("")

if imc < 18.5:
    print("Baxio")
elif 18.5 <= imc < 25:  # imc >= 18.5 and imc < 25
    print("Ideal")
elif 25 <= imc < 30:  # imc >= 25 and imc < 30
    print("Sobrepeso")
elif 30 <= imc < 40:  # imc >= 30 and imc < 40
    print("Obesidade")
else:
    print("Muita Obesidade")
